# good-life

A conway's game of life simulation. The following extra credit features are
supported:

1. Custom Size. If a size for the board is provided:

       ./good-life 100

   The board in the simulation will be of that size.

2. Blinker oscillator. If called as follows, a blinker will occur in the center
   of the grid. The size of the board can also be specified.

       ./good-life 25 --blinker

   An oscillator can also be observed when running with a board size of 50 and a
   count of 300 nodes.

       ./good-life 50 --count=300

3. Birth. If called as follows, there is a 1 in 8 chance that a dead node will
   be come to life.

      ./good-life --birth=8 --count=1

The following command line flags are available:

* `--blinker` Creates a blinker in the center of the board.
* `--birth=<birth_rate>` Gives dead nodes a 1/birth_rate chance of coming back
  to life.
* `--count` Specify the number of alive nodes to randomly place at the beginning.
* `[size]` The size of the board.
