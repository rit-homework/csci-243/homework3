#define _BSD_SOURCE
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "display.h"

const char ALIVE = '*';
const char DEAD = ' ';

/// A mod function. The `%` is the remainder function and defined as
///
///   a == (a / b * b) + a % b
///
/// Which allows the result to be negative. The result of this mod is always in
/// the range [0, rhs).
static int mod(int lhs, int rhs) { return ((lhs % rhs) + rhs) % rhs; }

/// Returns true when str starts with the prefix pre.
static bool has_prefix(const char *pre, const char *str) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

/// Returns true every one in n times.
static bool random_decision(int n) { return rand() % n == 0; }

/// Prints the board to standard output.
static void print_board(const int size, char board[size][size]) {
    set_cur_pos(1, 1);
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            put(board[row][col]);
        }

        put('\n');
    }
}

/// Initializes the board will all dead tiles.
static void clear_board(const int size, char board[size][size]) {
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            board[row][col] = DEAD;
        }
    }
}

/// Populates the board with count `organisms` placed randomly.
static void poulate_random_board(const int size, char board[size][size],
                                 const int organisms) {
    for (int i = 0; i < organisms; i++) {
        int row = rand();
        row %= size;

        int col = rand();
        col %= size;

        board[row][col] = ALIVE;
    }
}

/// A description for a direction a neighbor could be in.
typedef struct direction_t {
    int offset_y;
    int offset_x;
} Direction;

// All possible directions a neighbor could be found.
static const Direction directions[] = {
    {.offset_y = 1, .offset_x = 0},  {.offset_y = 1, .offset_x = 1},
    {.offset_y = 0, .offset_x = 1},  {.offset_y = -1, .offset_x = 1},
    {.offset_y = -1, .offset_x = 0}, {.offset_y = -1, .offset_x = -1},
    {.offset_y = 0, .offset_x = -1}, {.offset_y = 1, .offset_x = -1},
};

/// Counts the number of living to the tile at the zero-indexed row and col.
/// Wraps at the bounds.
static int count_neighbors_board(const int size, char board[size][size],
                                 const int row, const int col) {
    int neighbors = 0;
    for (unsigned int i = 0; i < sizeof(directions) / sizeof(directions[0]);
         i++) {
        Direction direction = directions[i];

        int check_row = mod(row + direction.offset_x, size);
        int check_col = mod(col + direction.offset_y, size);

        char neighbor = board[check_row][check_col];
        if (neighbor == ALIVE) {
            neighbors++;
        }
    }

    return neighbors;
}

/// Given the current state of a tile and the number of neighbors it has,
/// returns the next state the tile should be iin.
static char next_state(int neighbors, char current_state) {
    if (current_state == DEAD && neighbors == 3) {
        // Rule 2 on Wikipedia.
        return ALIVE;
    }

    if (current_state == ALIVE && neighbors >= 2 && neighbors <= 3) {
        // Rule 4 on Wikipedia.
        return ALIVE;
    }

    // All other cells die or remain dead.
    return DEAD;
}

/// Updates the passed in board (in-palce) to the next generation following
/// Wikipedia's rules for Conway's game of life.
static void next_generation_board(const int size, char board[size][size],
                                  const int *birth_rate) {
    char next_board[size][size];

    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            char current_state = board[row][col];
            int neighbors = count_neighbors_board(size, board, row, col);
            char next = next_state(neighbors, current_state);
            next_board[row][col] = next;
        }
    }

    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            if (birth_rate && board[row][col] == DEAD &&
                next_board[row][col] == DEAD && random_decision(*birth_rate)) {
                next_board[row][col] = ALIVE;
            }

            board[row][col] = next_board[row][col];
        }
    }
}

/// Adds a blinker to the middle of the board.
static void add_blinker(const int size, char board[size][size]) {
    int middle = size / 2;

    int lower_bound = middle - 2;
    int upper_bound = middle + 2;

    for (int row = lower_bound; row <= upper_bound; row++) {
        for (int col = lower_bound; col <= upper_bound; col++) {
            board[row][col] = DEAD;
        }
    }

    board[middle - 1][middle] = ALIVE;
    board[middle][middle] = ALIVE;
    board[middle + 1][middle] = ALIVE;
}

int main(int argc, char *argv[]) {
    // The size of the board.
    int size = 20;

    // Whether or not a blinker should be created.
    bool should_create_blinker = false;

    // Births happen one in this many times when possible.
    int *birth_frequency = NULL;

    // The number of organisms the user wants to populate the initial board
    // with.
    int *organisms = NULL;

    int organisms_input;

    // Seed random number generator
    unsigned int seed = 31;
    srand(seed);

    // Parse Arguments
    for (int i = 1; i < argc; i++) {
        char *arg = argv[i];

        if (!has_prefix("--", arg)) {
            // Parse Size Argument

            // Use default size if size is 0 or otherwise invalid.
            int arg_size = (int)strtol(arg, NULL, 10);
            if (arg_size) {
                size = arg_size;
            }
        } else {
            if (strcmp(arg, "--blinker") == 0) {
                if (size < 5) {
                    printf("Invalid Size (%d). Must be larger then 5.\n", size);
                }

                should_create_blinker = true;
            } else if (has_prefix("--birth=", arg)) {
                const size_t birth_frequency_prefix_size = strlen("--birth=");
                int birth_frequency_arg =
                    (int)strtol(arg + birth_frequency_prefix_size, NULL, 10);

                if (birth_frequency_arg) {
                    birth_frequency = &birth_frequency_arg;
                }
            } else if (has_prefix("--count=", arg)) {
                const size_t count_prefix_size = strlen("--count=");

                int count_arg = (int)strtol(arg + count_prefix_size, NULL, 10);

                if (count_arg) {
                    organisms = &count_arg;
                }
            }
        }
    }

    // The current state of the game. This is mutated in place.
    char board[size][size];

    if (organisms == NULL) {
        // Initialize With Desired State
        printf("\n\t..Welcome to the Game of life..\n");
        printf("\nPlease enter the initial number of organisms: ");
        scanf("%i", &organisms_input);
        organisms = &organisms_input;
    }
    clear();

    // Clear Board
    clear_board(size, board);
    poulate_random_board(size, board, *organisms);

    if (should_create_blinker) {
        add_blinker(size, board);
    }

    // Print Board
    print_board(size, board);

    for (int generation = 0; 1; generation++) {
        next_generation_board(size, board, birth_frequency);
        print_board(size, board);
        usleep(81000);

        printf("\ngeneration: %d\n", generation);
    }
}
