[me@falcon act1]$ gdb -q good-life
Reading symbols from good-life...done.
(gdb) run
Starting program: /home/me/school/csci243/homework3/act1/good-life 

	..Welcome to the Game of life..

Please enter the initial number of organisms: 123

Program received signal SIGSEGV, Segmentation fault.
0x00007ffff7b77f11 in __strlen_avx2 () from /usr/lib/libc.so.6
(gdb) where
#0  0x00007ffff7b77f11 in __strlen_avx2 () from /usr/lib/libc.so.6
#1  0x00007ffff7a6fded in vfprintf () from /usr/lib/libc.so.6
#2  0x00007ffff7a769d6 in printf () from /usr/lib/libc.so.6
#3  0x0000555555555032 in main () at good-life.c:176
(gdb) up
#1  0x00007ffff7a6fded in vfprintf () from /usr/lib/libc.so.6
(gdb) up
#2  0x00007ffff7a769d6 in printf () from /usr/lib/libc.so.6
(gdb) up
#3  0x0000555555555032 in main () at good-life.c:176
176					  printf("%s", life[row][col]);
(gdb) list
171				  
172			 for(row = 0; row<20; row++)
173			 {
174				  for(col = 0; col<20; col++)
175				  {
176					  printf("%s", life[row][col]);
177				  }
178				  puts(" ");
179			  }
180				 
(gdb) break
Breakpoint 1 at 0x555555555032: file good-life.c, line 174.
(gdb) info break
Num     Type           Disp Enb Address            What
1       breakpoint     keep y   0x0000555555555032 in main at good-life.c:174
(gdb) break 176
Breakpoint 2 at 0x555555554fed: file good-life.c, line 176.
(gdb) delete breakpoint 1
(gdb) info break
Num     Type           Disp Enb Address            What
2       breakpoint     keep y   0x0000555555554fed in main at good-life.c:176
(gdb) 
