#include <stdio.h>
#include <stdlib.h>

/// Every tile on the board is in one of these states.
typedef char State;
const State ALIVE = '*';
const State DEAD = ' ';

#define COLS 20
#define ROWS 20
State board[ROWS][COLS];

/// The number of generations to run.
const int generations = 100;

/// A mod function. The `%` is the remainder function and defined as
///
///   a == (a / b * b) + a % b
///
/// Which allows the result to be negative. The result of this mod is always in
/// the range [0, rhs).
int mod(int lhs, int rhs) { return ((lhs % rhs) + rhs) % rhs; }

/// Prints the board to standard output.
void print_board(State board[ROWS][COLS]) {
    for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
            printf("%c", board[row][col]);
        }
        puts(" ");
    }
}

/// Initializes the board will all dead tiles.
void clear_board(State board[ROWS][COLS]) {
    for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
            board[row][col] = DEAD;
        }
    }
}

/// Populates the board with count `organisms` placed randomly.
void poulate_random_board(State board[ROWS][COLS], const int organisms) {
    for (int i = 0; i < organisms; i++) {
        int row = rand();
        row %= ROWS;

        int col = rand();
        col %= COLS;

        board[row][col] = ALIVE;
    }
}

/// A description for a direction a neighbor could be in.
typedef struct direction_t {
    int offset_y;
    int offset_x;
} Direction;

/// The number of elements in the directions array.
#define DIRECTIONS_COUNT 8

// All possible directions a neighbor could be found.
const Direction directions[] = {
    {.offset_y = 1, .offset_x = 0},  {.offset_y = 1, .offset_x = 1},
    {.offset_y = 0, .offset_x = 1},  {.offset_y = -1, .offset_x = 1},
    {.offset_y = -1, .offset_x = 0}, {.offset_y = -1, .offset_x = -1},
    {.offset_y = 0, .offset_x = -1}, {.offset_y = 1, .offset_x = -1},
};

/// Counts the number of living to the tile at the zero-indexed row and col.
/// Wraps at the bounds.
int count_neighbors_board(State board[ROWS][COLS], const int row,
                          const int col) {
    int neighbors = 0;
    for (int i = 0; i < DIRECTIONS_COUNT; i++) {
        Direction direction = directions[i];

        int check_row = mod(row + direction.offset_x, ROWS);
        int check_col = mod(col + direction.offset_y, COLS);

        State neighbor = board[check_row][check_col];
        if (neighbor == ALIVE) {
            neighbors++;
        }
    }

    return neighbors;
}

/// Given the current state of a tile and the number of neighbors it has,
/// returns the next state the tile should be iin.
State next_state(int neighbors, State current_state) {
    if (current_state == DEAD && neighbors == 3) {
        // Rule 2 on Wikipedia.
        return ALIVE;
    }

    if (current_state == ALIVE && neighbors >= 2 && neighbors <= 3) {
        // Rule 4 on Wikipedia.
        return ALIVE;
    }

    // All other cells die or remain dead.
    return DEAD;
}

/// Updates the passed in board (in-palce) to the next generation following
/// Wikipedia's rules for Conway's game of life.
void next_generation_board(State board[ROWS][COLS]) {
    State next_board[ROWS][COLS];

    for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
            char current_state = board[row][col];
            int neighbors = count_neighbors_board(board, row, col);
            char next = next_state(neighbors, current_state);
            next_board[row][col] = next;
        }
    }

    for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
            board[row][col] = next_board[row][col];
        }
    }
}

int main(void) {
    // The number of organisms the user wants to populate the initial board
    // with.
    int organisms;

    // Print Header and Request Organisms
    printf("\n\t..Welcome to the Game of life..\n");
    printf("\nPlease enter the initial number of organisms: ");
    scanf("%i", &organisms);

    // Seed random number generator
    unsigned int seed = 31;
    srand(seed);

    // Initialize Board With Random Organisms
    clear_board(board);
    poulate_random_board(board, organisms);

    // Print Board
    print_board(board);

    for (int generation = 0; generation < generations; generation++) {
        next_generation_board(board);
        print_board(board);

        printf("\ngeneration: %d\n", generation);
    }

    return 0;
}
